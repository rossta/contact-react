### ContactReact

ContactReact is an online database for your contacts. This is a demo project by
[@rossta](https://rossta.net).

##### Features

* A visitor can upload and import tab-delimited files via AJAX at [/contacts](/contacts).
* Parseable files contain the following columns: `first_name`, `last_name`, `email_address`, `phone_number`, `company_name`. An example file can be [downloaded here](/data.tsv).
* Contact records are extracted from the import files and displayed at [/contacts](/contacts).
* Individual contacts can be deleted via AJAX.
* The contact list can be filtered by various criteria, including:
  * Only contacts with international numbers
  * Only contacts numbers with an extension
  * Only contacts with `.com` email addresses
* The contact list can be ordered alphabetically by first name, last name, email address, and company name

##### Tools

* The application server logic is written in [Ruby on Rails](http://rubyonrails.org/)
* The client application is built in [React](https://facebook.github.io/react/) with [Redux](http://redux.js.org/)
* JavaScript code is written in ES2015 syntax with transpiling support provided
  by `browserify-rails`.
* The client CSS and plugin themes are provided by [Materialize](http://materializecss.com/)
* Contact info is stored in Postgres and import data is stored in Rails.cache backed by Memcached
* The Pusher service is used to push data to the client
* The app is deployed on Heroku and built on CircleCi

##### Notes

* The React client has three main "views": the Dashboard that lists contacts, the Import dialog for uploading TSV, and the Import confirmation page for previewing the import before contact info is saved to the Postgres database. Each "view" corresponds to a URL managed by React-Router.
* The import runs in the background with a Rails Active Job (using `sucker_punch`). When the import complete in the background, the status is "pushed" to the client via Pusher, which triggers a refresh of the client contacts.
* Import and pending contact data is backed by ActiveModel PORO objects with validations and are stored in Rails.cache. Phone number validation is supported via the `phony` gem. File size is only validated on the front-end, but ideally should be validated on the back-end as well (never trust the client!).
* Batch importing is provided by `activerecord-import` gem for more efficient inserts to Postgres once pending contacts are confirmed.
* The import executes validations on contact info but does not yet communicate them back to the client. A future enhancement would be to display the errors inline on the import confirmation page with editable content inputs for users the edit the import by hand before confirming.

