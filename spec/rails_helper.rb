# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
# Prevent database truncation if the environment is production
abort('The Rails environment is running in production mode!') if Rails.env.production?
require 'spec_helper'
require 'rspec/rails'
# Add additional requires below this line. Rails is not loaded until this point!
require 'sucker_punch/testing/inline'

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
#
# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.
#
# Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

# Checks for pending migration and applies them before tests are run.
# If you are not using ActiveRecord, you can remove this line.
ActiveRecord::Migration.maintain_test_schema!

require 'capybara/poltergeist'
Capybara.register_driver :poltergeist do |app|
  Capybara::Poltergeist::Driver.new(app, timeout: 1.minute, phantomjs_options: ['--load-images=no'])
end
Capybara.javascript_driver = :poltergeist

RSpec.configure do |config|
  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = false

  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, :type => :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!

  # Filter lines from Rails gems in backtraces.
  config.filter_rails_from_backtrace!
  # arbitrary gems may also be filtered via:
  # config.filter_gems_from_backtrace("gem name")

  config.before(:suite) do
    DatabaseCleaner.strategy = :truncation
  end

  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end

  config.around(:each, :caching) do |example|
    caching = ActionController::Base.perform_caching
    ActionController::Base.perform_caching = example.metadata[:caching]

    example.run

    Rails.cache.clear
    ActionController::Base.perform_caching = caching
  end
end

def create_import_fixture
  file = File.open(Rails.root.join('spec/fixtures/contacts.tsv'), 'r')
  Import.from_file(file)
end

def create_contacts_fixture
  Contact.create_from_import(create_import_fixture)
end

def contacts_rows
  [
    ['Myra', 'Crona', 'myracrona@schinner.info', '(724)196-9470 x998', 'Champlin-Hahn'],
    ['Josh', 'Donnelly', 'joshdonnelly@macejkovic.us', '081-799-3139 x248', 'Casper Group'],
    ['Verna', 'Farrell', 'vernafarrell@schillercorkery.name', '731.101.6219', ' Rosenbaum-Hane']
  ]
end

def contacts_attributes
  contacts_rows.map do |attrs|
    Hash[Contact.display_column_names.zip(attrs)]
  end
end

def contact_1_attributes
  contacts_attributes.take(1)
end

def expect_all_contacts_to_be_visible
  within('table#contacts') do
    %w(Gerhard Myra Josh Verna Julian).each do |name|
      expect(page).to have_content(name)
    end

    expect(page).to have_css('tr.contact', count: 5)
  end
end

def expect_contact_row_order(rows)
  rows.each.with_index do |content, i|
    within_nth_contacts_table_row(i + 1) do
      expect(page).to have_content(content),
                      "Expected row #{i + 1} to have content #{content.inspect}"
    end
  end
end

def within_nth_contacts_table_row(num, &block)
  within('table#contacts') do
    within("tr.contact:nth-child(#{num})", &block)
  end
end
