require 'rails_helper'

describe HasContactInfo do
  shared_examples_for 'has contact info' do
    let(:contact) { new_contact }

    it 'should have an identifier present' do
      contact = new_contact(first_name: '', last_name: '', email_address: '')
      expect(contact).to_not be_valid
      expect(contact.errors[:base]).to include(
        'Please provide at least one: First name, Last name, Email address'
      )
    end

    describe '#phone_number' do
      it 'parses (724)196-9470 x998' do
        contact = new_contact(phone_number: '(724)196-9470 x998')
        expect(contact).to be_valid
        expect(contact.phone_number).to eq('+17241969470 x998')
      end

      it 'parses 081-799-3139 x248' do
        contact = new_contact(phone_number: '081-799-3139 x248')
        expect(contact).to be_valid
        expect(contact.phone_number).to eq('+10817993139 x248')
      end

      it 'parses 731.101.6219' do
        contact = new_contact(phone_number: '731.101.6219')
        expect(contact).to be_valid
        expect(contact.phone_number).to eq('+17311016219')
      end

      it 'parses 1-033-511-1831 x471' do
        contact = new_contact(phone_number: '1-033-511-1831 x471')
        expect(contact).to be_valid
        expect(contact.phone_number).to eq('+10335111831 x471')
      end
    end

    describe '#email_address' do
      it 'validates email' do
        contact = new_contact(email_address: 'bob@example.com')

        expect(contact).to be_valid
        expect(contact.email_address).to eq('bob@example.com')
      end

      it 'validates presence of @' do
        contact = new_contact(email_address: 'bob%example.com')

        expect(contact).to_not be_valid
      end
    end

    def valid_attrs
      { first_name: 'Hoss' }
    end
  end

  describe Contact do
    it_behaves_like 'has contact info'

    def new_contact(attrs = {})
      Contact.new(valid_attrs.merge(attrs))
    end
  end

  describe PendingContact do
    it_behaves_like 'has contact info'

    def new_contact(attrs = {})
      PendingContact.new(valid_attrs.merge(attrs))
    end
  end
end
