require 'rails_helper'

describe PendingContact do
  describe '#phone_number' do
    it 'parses (724)196-9470 x998' do
      contact = contact_phone_number(phone_number: '(724)196-9470 x998')
      expect(contact).to be_valid
      expect(contact.phone_number).to eq('+17241969470 x998')
    end

    it 'parses 081-799-3139 x248' do
      contact = contact_phone_number(phone_number: '081-799-3139 x248')
      expect(contact).to be_valid
      expect(contact.phone_number).to eq('+10817993139 x248')
    end

    it 'parses 731.101.6219' do
      contact = contact_phone_number(phone_number: '731.101.6219')
      expect(contact).to be_valid
      expect(contact.phone_number).to eq('+17311016219')
    end

    it 'parses 1-033-511-1831 x471' do
      contact = contact_phone_number(phone_number: '1-033-511-1831 x471')
      expect(contact).to be_valid
      expect(contact.phone_number).to eq('+10335111831 x471')
    end

    def contact_phone_number(phone_number:)
      new_pending_contact(phone_number: phone_number)
    end
  end

  describe '#email_address' do
    it 'validates email' do
      contact = contact_email_address(email_address: 'bob@example.com')

      expect(contact).to be_valid
      expect(contact.email_address).to eq('bob@example.com')
    end

    it 'validates presence of @' do
      contact = contact_email_address(email_address: 'bob%example.com')

      expect(contact).to_not be_valid
    end

    def contact_email_address(email_address:)
      new_pending_contact(email_address: email_address)
    end
  end

  def valid_attrs
    { first_name: 'Boss' }
  end

  def new_pending_contact(attrs)
    PendingContact.new(valid_attrs.merge(attrs))
  end
end
