require 'rails_helper'

describe Import do
  let(:file) { File.open(Rails.root.join('spec/fixtures/contacts.tsv'), 'r') }

  describe 'self.from_file' do
    it 'parses tab-delimited file' do
      # First Name	Last Name	Email Address	Phone Number	Company Name
      # Gerhard	Kautzer	gerhardkautzer@cronabayer.com	1-207-643-1816	Hodkiewicz-Lynch
      # Myra	Crona	myracrona@schinner.info	(724)196-9470 x998	Champlin-Hahn
      # Josh	Donnelly	joshdonnelly@macejkovic.us	081-799-3139 x248	Casper Group
      # Verna	Farrell	vernafarrell@schillercorkery.name	731.101.6219	Rosenbaum-Hane
      # Lauriane	Stracke	laurianestracke@tremblayturner.biz	1-033-511-1831 x471	Prohaska-Sporer
      import = Import.from_file(file)

      expect(import.size).to eq(5)
      expect(import.uuid).to match(/[a-z0-9]+/)

      contact_1 = import.first

      expect(contact_1.first_name).to eq('Gerhard')
      expect(contact_1.last_name).to eq('Kautzer')
      expect(contact_1.email_address).to eq('gerhardkautzer@cronabayer.com')
      expect(contact_1.phone_number).to eq('+12076431816')
      expect(contact_1.company_name).to eq('Hodkiewicz-Lynch')
    end

    it 'caches import result for locating later' do
      import = create_import_fixture
      found_import = Import.find(import.uuid)

      expect(found_import.size).to eq(5)
      expect(found_import.uuid).to match(/[a-z0-9]+/)
    end
  end

  describe 'self.create' do
    it 'caches result' do
      import = Import.create(pending_contacts: contact_1_attributes)
      attrs = Rails.cache.read(['import', import.uuid])

      expect(attrs['pending_contacts'].size).to eq(1)
      expect(attrs['pending_contacts'].first.first_name).to eq('Myra')
      expect(attrs['uuid']).to match(/[a-z0-9]+/)
    end
  end

  describe 'self.find', :caching do
    it 'finds cached import' do
      uuid = SecureRandom.uuid
      import = Import.new(uuid: uuid, pending_contacts: contact_1_attributes)
      Rails.cache.write(['import', uuid], import.as_json)
      import = Import.find(uuid)

      expect(import.size).to eq(1)
      expect(import.first.first_name).to eq('Myra')
      expect(import.uuid).to eq(uuid)
    end

    it 'raises not found error' do
      expect { Import.find('nonsense') }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe '#confirm' do
    it 'creates contacts' do
      import = Import.create(pending_contacts: create_import_fixture.pending_contacts)

      import = Import.find(import.uuid)

      import.confirm

      expect(Contact.count).to eq(5)
    end

    it 'is idempotent' do
      import = Import.create(pending_contacts: Import.from_file(file).pending_contacts)

      import.confirm
      import.confirm

      import = Import.find(import.uuid)

      import.confirm

      expect(Contact.count).to eq(5)
    end
  end
end
