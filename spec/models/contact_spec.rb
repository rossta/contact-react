require 'rails_helper'

RSpec.describe Contact, type: :model do
  describe 'self.create_from_import' do
    it 'bulk creates contacts from import object' do
      import = Import.new(pending_contacts: contacts_attributes)

      Contact.create_from_import(import)

      expect(Contact.count).to eq(3)

      contact = Contact.first
      expect(contact.first_name).to eq('Myra')
      expect(contact.last_name).to eq('Crona')
      expect(contact.email_address).to eq('myracrona@schinner.info')
      expect(contact.phone_number).to eq('+17241969470 x998')
      expect(contact.company_name).to eq('Champlin-Hahn')
    end
  end
end
