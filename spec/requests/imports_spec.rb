require 'rails_helper'

RSpec.describe 'Imports', :caching, type: :request do
  describe 'GET /contacts/import/:id' do
    it 'succeeds for existing import' do
      import = Import.create(pending_contacts: contact_1_attributes)

      get import_path(import.uuid), {}, 'ACCEPT' => 'application/json'
      expect(response).to have_http_status(200)

      import_json = import.to_json(
        root: true,
        include: { pending_contacts: { methods: :phone_number_formatted } }
      )
      expect(response.body).to eq(import_json)
    end

    it 'fails for non-existant import' do
      expect do
        get import_path('nonsense'), 'ACCEPT' => 'application/json'
      end.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
