require 'rails_helper'

RSpec.describe 'Contacts', type: :request do
  describe 'GET /contacts' do
    it 'is a success' do
      get contacts_path
      expect(response).to have_http_status(200)
    end

    it 'responds to json' do
      Contact.create(contacts_attributes)
      contacts = Contact.order(first_name: :asc)
      get contacts_path, {}, 'ACCEPT' => 'application/json'
      expect(response).to have_http_status(200)

      contacts_json = { contacts: contacts.as_json(methods: [:phone_number_formatted]) }.to_json
      expect(response.body).to eq(contacts_json)
    end
  end
end
