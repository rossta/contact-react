require 'rails_helper'

RSpec.feature 'Import contacts', type: :feature do
  scenario 'visitor imports contacts by tsv upload', :js, :job do
    visit root_path
    click_link 'Go to Dashboard'

    within('main') do
      expect(page).to have_content('Contacts')
      click_link 'Import'
    end

    expect(page).to have_content('Import Contacts')
    expect(page).to have_content('Choose the file')

    within '#modal' do
      attach_file 'file', Rails.root.join('spec/fixtures/contacts.tsv')
      click_button 'Upload'
    end

    expect(page).to have_content('Please confirm')

    expect(page).to have_content('Gerhard')
    expect(page).to have_content('Kautzer')
    expect(page).to have_content('gerhardkautzer@cronabayer.com')
    expect(page).to have_content('Hodkiewicz-Lynch')

    click_link 'Finish import'

    expect(page).to have_content("We're working on your import")
    expect(Contact.count).to eq(5)
  end
end
