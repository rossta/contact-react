require 'rails_helper'

RSpec.feature 'Delete contacts', type: :feature do
  scenario 'visitor deletes a contact', :js do
    create_contacts_fixture

    visit root_path
    click_link 'Go to Dashboard'

    expect(page).to have_css('table#contacts')

    within('table#contacts') do
      expect(page).to have_css('tr.contact', count: 5)
    end

    within('table#contacts tr.contact:first-child') do
      expect(page).to have_content('Gerhard')
      expect(page).to have_content('Kautzer')

      click_link 'Delete'
    end

    within('table#contacts') do
      expect(page).to_not have_content('Gerhard')
      expect(page).to_not have_content('Kautzer')
      expect(page).to have_css('tr.contact', count: 4)
    end

    within('table#contacts tr.contact:last-child') do
      expect(page).to have_content('Verna')
      expect(page).to have_content('Farrell')

      click_link 'Delete'
    end

    within('table#contacts') do
      expect(page).to_not have_content('Verna')
      expect(page).to_not have_content('Farrell')
      expect(page).to have_css('tr.contact', count: 3)
    end
  end
end
