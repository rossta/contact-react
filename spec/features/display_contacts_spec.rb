require 'rails_helper'

RSpec.feature 'Display contacts', :js, type: :feature do
  context 'visitor visits dashboard' do
    scenario 'sees blank slate' do
      visit root_path
      click_link 'Go to Dashboard'

      expect(page).to_not have_css('table#contacts')
      expect(page).to have_content("You don't have any contacts yet!")

      click_link 'Import your contact list'
      expect(page).to have_css('#modal')
    end

    scenario 'sees contacts' do
      create_contacts_fixture

      visit root_path
      click_link 'Go to Dashboard'

      expect(page).to have_css('table#contacts')
      expect_all_contacts_to_be_visible

      within('table#contacts') do
        expect(page).to have_content('Gerhard')
        expect(page).to have_content('Kautzer')
        expect(page).to have_content('gerhardkautzer@cronabayer.com')
        expect(page).to have_content('+1-(207)-643-1816')
        expect(page).to have_content('Hodkiewicz-Lynch')
      end
    end
  end

  context 'visitor filters contacts' do
    before do
      create_contacts_fixture

      visit root_path
      click_link 'Go to Dashboard'

      expect(page).to have_css('table#contacts')
      expect_all_contacts_to_be_visible
    end

    scenario 'with international numbers' do
      select 'Phone number: international', from: 'filter-contacts'

      within('table#contacts') do
        expect(page).to have_content('Julian')

        expect(page).to_not have_content('Gerhard')
        expect(page).to_not have_content('Myra')
        expect(page).to_not have_content('Josh')
        expect(page).to_not have_content('Verna')

        expect(page).to have_css('tr.contact', count: 1)
      end

      select 'All', from: 'filter-contacts'
      expect_all_contacts_to_be_visible
    end

    scenario 'numbers with an extension' do
      select 'Phone number: extension', from: 'filter-contacts'

      within('table#contacts') do
        expect(page).to have_content('Julian')
        expect(page).to have_content('Myra')
        expect(page).to have_content('Josh')

        expect(page).to_not have_content('Gerhard')
        expect(page).to_not have_content('Verna')

        expect(page).to have_css('tr.contact', count: 3)
      end
      select 'All', from: 'filter-contacts'
      expect_all_contacts_to_be_visible
    end

    scenario 'with .com email addresses' do
      select 'Email address: .com', from: 'filter-contacts'

      within('table#contacts') do
        expect(page).to have_content('Gerhard')

        expect(page).to_not have_content('Myra')
        expect(page).to_not have_content('Josh')
        expect(page).to_not have_content('Verna')
        expect(page).to_not have_content('Julian')

        expect(page).to have_css('tr.contact', count: 1)
      end

      select 'All', from: 'filter-contacts'
      expect_all_contacts_to_be_visible
    end
  end

  context 'visitor re-orders contacts' do
    before do
      create_contacts_fixture

      visit root_path
      click_link 'Go to Dashboard'

      expect(page).to have_css('table#contacts')
      expect_all_contacts_to_be_visible
    end

    scenario 'alphabetically by email address' do
      within('table#contacts') do
        click_link 'Email address'
      end

      expect_contact_row_order [
        'gerhardkautzer@cronabayer.com',
        'joshdonnelly@macejkovic.us',
        'julianorn@altenwerth.co.uk',
        'myracrona@schinner.info',
        'vernafarrell@schillercorkery.name'
      ]

      within('table#contacts') do
        click_link 'Email address'
      end

      expect_contact_row_order [
        'vernafarrell@schillercorkery.name',
        'myracrona@schinner.info',
        'julianorn@altenwerth.co.uk',
        'joshdonnelly@macejkovic.us',
        'gerhardkautzer@cronabayer.com'
      ]

      within('table#contacts') do
        click_link 'First name'
      end

      expect_contact_row_order %w(
        Gerhard
        Josh
        Julian
        Myra
        Verna
      )

      within('table#contacts') do
        click_link 'First name'
      end

      expect_contact_row_order %w(
        Verna
        Myra
        Julian
        Josh
        Gerhard
      )
    end
  end
end
