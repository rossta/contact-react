require 'rails_helper'

RSpec.feature 'Welcomes', type: :feature do
  scenario 'visitor visit home page' do
    visit root_path

    expect(page).to have_content('ContactReact')
    expect(page).to have_content('by @rossta')
  end
end
