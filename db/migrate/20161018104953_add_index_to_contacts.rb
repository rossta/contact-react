class AddIndexToContacts < ActiveRecord::Migration
  def change
    add_index :contacts, :first_name
  end
end
