class ImportsController < ApplicationController
  # Display an Import by UUID given as params :id
  #
  def show
    import = Import.find(params[:id])

    respond_to do |format|
      format.json do
        render json: import_json(import)
      end

      format.html do
        render template: 'contacts/index'
      end
    end
  end

  # Return react app for react router
  #
  def new
    render template: 'contacts/index'
  end

  # Creates an import via :file upload, renders Import JSON
  #
  def create
    import = Import.from_file(params[:file])

    respond_to do |format|
      format.json do
        render json: import_json(import)
      end
    end
  end

  # Converts Import data to Contact records
  #
  def confirm
    import = Import.find(params[:id])

    ConfirmImportJob.perform_later(import)

    render json: { notice: "We're working on your import..." }
  end

  private

  def import_json(import)
    import.as_json(
      include: {
        pending_contacts: {
          methods: [:phone_number_formatted]
        }
      },
      root: true
    )
  end
end
