class ContactsController < ApplicationController
  # Lists contacts via JSON, renders react app via HTML
  #
  def index
    respond_to do |format|
      format.html do
        render template: 'contacts/index'
      end

      format.json do
        render json: { contacts: Contact.order(first_name: :asc).as_json(json_options) }
      end
    end
  end

  # Deletes a contact params :id
  #
  def destroy
    contact = Contact.find(params[:id])

    contact.destroy

    render json: contact.as_json(json_options.merge(root: true)).merge(
      notice: "Contact for #{contact.email_address} has been deleted ;)"
    )
  end

  private

  def json_options
    { methods: [:phone_number_formatted] }
  end
end
