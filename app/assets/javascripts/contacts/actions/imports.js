import _ from 'lodash';
import { push } from 'react-router-redux';
import Constants from '../constants';
import { postOptions } from '../utils';
import { toJson, status } from './shared';
import logger from '../utils/logger';

function requestImport(uuid) {
  return {
    type: Constants.IMPORTS_REQUEST_IMPORT,
    uuid,
  }
}

function receiveImport(importJson, dispatch) {
  const { uuid, pending_contacts } = importJson;

  dispatch(push(`/contacts/import/${uuid}`))

  return {
    type: Constants.IMPORTS_RECEIVE_IMPORT,
    pendingContacts: pending_contacts,
    uuid,
  }
}

function importNotFound(uuid) {
  return {
    type: Constants.IMPORTS_NOT_FOUND,
    uuid,
  };
}

function fetchImport(uuid) {
  return dispatch => {
    dispatch(requestImport(uuid));
    return fetch(`/contacts/import/${uuid}`)
    .then(status)
    .then(toJson)
    .then(json => dispatch(receiveImport(json.import, dispatch)))
  }
}

function shouldFetchImport(state) {
  const imports = state.imports;
  if (!imports.uuid && _.isEmpty(imports.pendingContacts)) {
    return true;
  } else if (imports.isFetching) {
    return false;
  } else {
    return imports.notFound;
  }
}

export function fetchImportIfNeeded(uuid) {
  return (dispatch, getState) => {
    if (shouldFetchImport(getState())) {
      return dispatch(fetchImport(uuid))
    }
  }
}

export function uploadFile(file) {
  return dispatch => {
    const options = postOptions();
    const formData = new FormData();
    formData.append('file', file);
    options.body = formData;

    return fetch('/contacts/import', options)
    .then(status)
    .then(toJson)
    .then(json => dispatch(receiveImport(json.import, dispatch)))
    .catch(e => {
      logger.log('Error occurred during upload', e);
    });
  }
}
