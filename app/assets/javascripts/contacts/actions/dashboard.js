import { push } from 'react-router-redux';
import Constants from '../constants';

export function newImport() {
  return dispatch => {
    dispatch(push('/import/new'));
  }
}

export function closeModal() {
  return dispatch => {
    dispatch(push("/contacts"));
  }
}

export function showFlashMessage(notice) {
  return {
    type: Constants.DASHBOARD_FLASH_SHOW,
    notice,
  };
}

export function hideFlashMessage() {
  return {
    type: Constants.DASHBOARD_FLASH_HIDE,
  };
}
