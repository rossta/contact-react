import _ from 'lodash';
import { push } from 'react-router-redux';
import Constants from '../constants';
import { postOptions, deleteOptions, getOptions } from '../utils';
import logger from '../utils/logger';
import { toJson, status } from './shared';
import { showFlashMessage } from './dashboard';

function requestContacts() {
  return {
    type: Constants.CONTACTS_REQUEST_CONTACTS,
  };
}

function receiveContacts(json) {
  return {
    type: Constants.CONTACTS_RECEIVE_CONTACTS,
    contacts: json.contacts,
  };
}

function willDeleteContact(contact) {
  return {
    type: Constants.CONTACTS_WILL_DELETE,
    contact,
  };
}

function didDeleteContact(contact) {
  return {
    type: Constants.CONTACTS_DID_DELETE,
    contact,
  };
}

function didConfirmImport() {
  return {
    type: Constants.CONTACTS_DID_CONFIRM,
  };
}

export function filterContacts(filter) {
  return {
    type: Constants.CONTACTS_SET_FILTER,
    filter,
  };
}

export function reorderContacts(property, direction) {
  const order = {};
  order[property] = direction;
  return {
    type: Constants.CONTACTS_SET_ORDER,
    order,
  };
}

export function fetchContacts() {
  return dispatch => {
    dispatch(requestContacts());
    return fetch('/contacts', getOptions())
    .then(status)
    .then(toJson)
    .then(json => dispatch(receiveContacts(json)))
  };
}

function shouldFetchContacts(state) {
  return (!state.isImporting && _.isEmpty(state.contacts.contacts));
}

export function fetchContactsIfNeeded() {
  return (dispatch, getState) => {
    if (shouldFetchContacts(getState())) {
      dispatch(fetchContacts())
    }
  };
}

export function confirmImport(importUuid, channel) {
  return dispatch => {
    return fetch(`/imports/${importUuid}/confirm`, postOptions())
    .then(status)
    .then(toJson)
    .then(json => {
      dispatch(didConfirmImport());
      dispatch(push('/contacts'));
      dispatch(showFlashMessage(json.notice));
    })
    .catch(e => {
      logger.log('Error occurred during request for contacts', e);
    });
  }
}

export function deleteContact(contact) {
  return dispatch => {
    dispatch(willDeleteContact(contact));
    return fetch(`/contacts/${contact.id}`, deleteOptions())
    .then(status)
    .then(toJson)
    .then(json => {
      dispatch(showFlashMessage(json.notice));
      dispatch(didDeleteContact(contact));
    })
    .catch(e => {
      logger.log('Error occurred during request for contacts', e);
    });
  }
}
