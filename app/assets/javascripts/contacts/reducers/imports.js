import Constants from '../constants';

const initialState = {
  pendingContacts: [],
  isFetching: false,
  notFound: false,
  uuid: null,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case Constants.IMPORTS_REQUEST_IMPORT:
      return {
        ...initialState,
        isFetching: true,
      }
    case Constants.IMPORTS_RECEIVE_IMPORT:
      return {
        ...initialState,
        pendingContacts: action.pendingContacts,
        uuid: action.uuid,
      };
    case Constants.IMPORTS_NOT_FOUND:
      return {
        ...initialState,
        notFound: true,
      }
    default:
      return state;
  }
}
