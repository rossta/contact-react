import Constants from '../constants';

Pusher.logToConsole = true;
const pusher = new Pusher(window.PUSHER_KEY, {
  encrypted: true
});

const channel = pusher.subscribe('contacts_channel');

const initialState = {
  notice: null,
  channel,
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case Constants.DASHBOARD_FLASH_SHOW:
      return {
        ...state,
        notice: action.notice,
      };
    case Constants.DASHBOARD_FLASH_HIDE:
      return {
        ...state,
        notice: null,
      };
    default:
      return initialState;
  }
}
