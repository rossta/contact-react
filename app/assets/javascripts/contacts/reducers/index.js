import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import dashboard from './dashboard';
import imports from './imports';
import contacts from './contacts';

export default combineReducers({
  routing: routerReducer,
  dashboard: dashboard,
  imports: imports,
  contacts: contacts,
});
