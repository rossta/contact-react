import _ from 'lodash';
import Constants from '../constants';

const initialState = {
  contacts: [],
  isFetching: false,
  isDeleting: [],
  isImporting: false,
  filter: null,
  order: {},
};

export default function reducer(state = initialState, action = {}) {
  let contact, contacts;
  switch (action.type) {
    case Constants.CONTACTS_REQUEST_CONTACTS:
      return {
        ...state,
        isImporting: false,
        isFetching: true,
      };
    case Constants.CONTACTS_RECEIVE_CONTACTS:
      return {
        ...state,
        isImporting: false,
        isFetching: false,
        contacts: action.contacts,
      };
    case Constants.CONTACTS_WILL_DELETE:
      contact = _.find(state.contacts, (c) => c.id === action.contact.id);
      if (contact) { contact.disabled = true; }

      return {
        ...state,
        isDeleting: [...state.isDeleting, contact.id]
      };
    case Constants.CONTACTS_DID_DELETE:
      contact = action.contact;
      return {
        ...state,
        contacts: _.reject(state.contacts, { id: contact.id }),
        isDeleting: _.difference(state.isDeleting, [contact.id])
      };
    case Constants.CONTACTS_DID_CONFIRM:
      return {
        ...state,
        isImporting: true
      };
    case Constants.CONTACTS_SET_FILTER:
      return {
        ...state,
        filter: action.filter,
      };
    case Constants.CONTACTS_SET_ORDER:
      return {
        ...state,
        order: action.order,
      };
    default:
      return state;
  }
}
