import { IndexRoute, Route } from 'react-router';
import React from 'react';
import MainLayout from '../layouts/main';
import DashboardIndexView from '../views/dashboard';
import NewImportView from '../views/imports/new';
import ShowImportView from '../views/imports/show';
import NotFoundView from '../views/errors/not_found';

export default function configRoutes(store) {
  return (
    <Route path="/contacts" component={MainLayout}>
      <IndexRoute component={DashboardIndexView} />
      <Route path="/contacts" component={DashboardIndexView}>
        <Route path="import/new" component={NewImportView} />
      </Route>

      <Route path="import/:uuid" component={ShowImportView} />

      <Route path="*" component={NotFoundView} />
    </Route>
  );
}
