import React from 'react';
import { Link } from 'react-router';
import { connect  } from 'react-redux';

class NotFoundView extends React.Component {
  render() {
    const { dispatch } = this.props;

    return (
      <div id="not_found">
        <h3>Oops! (404)</h3>
        <p>Sorry, looks like you ended up out of bounds</p>
        <p>Let's get you <Link to="/contacts">back to the Dashboard</Link></p>
      </div>
    );
  }
}

const mapStateToProps = (state) => (
  state.dashboard
);

export default connect(mapStateToProps)(NotFoundView);
