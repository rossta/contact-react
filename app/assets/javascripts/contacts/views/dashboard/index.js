import _ from 'lodash';
import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import logger from '../../utils/logger';
import { hideFlashMessage } from '../../actions/dashboard';
import { setDocumentTitle } from '../../utils';

import {
  Header,
  FlashMessage,
  ContactsFilterDropdown,
  ContactsList,
} from '../../components';

// React smart component view rendered at /contacts
class DashboardIndexView extends React.Component {
  componentDidMount() {
    setDocumentTitle('Contacts');
  }

  onFlashMessageFinish() {
    const { dispatch } = this.props;

    dispatch(hideFlashMessage());
  }

  render() {
    const { dispatch, notice, isImporting } = this.props;

    return (
      <div id="home-index">
        {
          notice &&
            <FlashMessage notice={notice} onFinish={::this.onFlashMessageFinish} />
        }
        <div className="row header-row">
          <div className="col s12 l10">
            <Header>Contacts</Header>
          </div>
          <div className="col s12 l2">
            <Link to="/contacts/import/new"
              disabled={isImporting}
              className="import-button btn waves-effect waves-light margin-top"
              >Import</Link>
          </div>
        </div>

        { (!isImporting ||  _.some(this.props.contacts)) &&
          <div className="row controls-row">
            <div className="col s12 l6">
              <ContactsFilterDropdown dispatch={dispatch} />
            </div>
          </div>
        }

        <div className="row">
          <div className="col l12 s12">
            <ContactsList {...this.props} />
          </div>
        </div>
        <div className="row">
          {this.props.children}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { ...state.dashboard, ...state.contacts }
};

export default connect(mapStateToProps)(DashboardIndexView);
