import React from 'react';
import { connect } from 'react-redux';

import { setDocumentTitle } from '../../utils';

import {
  Header,
  Loader,
  ImportMissing,
  ImportConfirmation,
} from '../../components';

// React smart component view for displaying parsed import
class ShowImportView extends React.Component {
  componentDidMount() {
    setDocumentTitle('Confirm Contacts');
  }

  renderImport() {
    const { isFetching, notFound } = this.props;

    if (isFetching) {
      return <Loader>Processing your import...</Loader>;
    } else if (notFound) {
      return <ImportMissing />;
    } else {
      return <ImportConfirmation {...this.props} />;
    }
  }

  render() {
    return (
      <div id="import-show" className="row">
        <div className="col l12 s12">
          {this.renderImport()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { ...state.imports };
}

export default connect(mapStateToProps)(ShowImportView);
