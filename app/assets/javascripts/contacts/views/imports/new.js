import React from 'react';
import { connect } from 'react-redux';

import { setDocumentTitle } from '../../utils';
import { closeModal } from '../../actions/dashboard';
import { Modal, ImportUploadForm } from '../../components';

// React smart component view for new import modal
class NewImportView extends React.Component {
  render() {
    const { dispatch } = this.props;

    return (
      <Modal
        dispatch={dispatch}
        onClose={closeModal}
        >
        <div id="import-new">
          <ImportUploadForm dispatch={dispatch} />
        </div>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => {
  return { ...state.imports }
};

export default connect(mapStateToProps)(NewImportView);
