const US_COUNTRY_CODE = "1";
const COUNTRY_CODE_REGEX = /^\+(\d+)\-/;

// Returns true for contacts by international phone
export function hasPhoneNumberInternational(contact) {
  return contact.phone_number_formatted.match(COUNTRY_CODE_REGEX)[1] !== US_COUNTRY_CODE;
}

// Returns true for contacts by phone extension
export function hasPhoneNumberExtension(contact) {
  return !!contact.phone_number_formatted.match(/\s(x[\d]+)$/);
}

// Returns true for contacts with dot com emails
export function hasDotComEmailAddress(contact) {
  return contact.email_address.endsWith('.com');
}

