export { postOptions, deleteOptions, getOptions } from './request_options';
export {
  hasPhoneNumberInternational,
  hasPhoneNumberExtension,
  hasDotComEmailAddress,
} from './filters';

export function setDocumentTitle(title) {
  document.title = `${title} - ContactReact`;
}
