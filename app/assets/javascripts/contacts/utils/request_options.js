import _ from 'lodash';

// Returns headers for POST/DELETE fetch requests
function formHeaders(options) {
  let headers;

  if (options === 'json') {
    headers = jsonHeaderOptions();
  } else {
    headers = xhrHeaderOptions();
  }

  return new Headers(headers);
}

// Returns options for POST fetch requests
export function postOptions(options) {
  return {
    method: 'POST',
    headers: formHeaders(options),
    credentials: 'include',
  };
}

// Returns options for DELETE fetch requests
export function deleteOptions() {
  return {
    method: 'DELETE',
    headers: formHeaders('json'),
    credentials: 'include',
  };
}

// Returns options for GET fetch requests
export function getOptions() {
  return { headers: jsonHeaderOptions() };
}

// Returns XHR Headers for Rails
function xhrHeaderOptions() {
  return {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-Token': authenticityToken(),
  };
}

// Returns headers for JSON content types
function jsonHeaderOptions() {
  return _.merge(xhrHeaderOptions(), {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  });
}

// Returns the current Rails authenticity token
function authenticityToken() {
  const meta = document.querySelector('meta[name=csrf-token]');

  if (!meta) return "";

  return meta.content;
}
