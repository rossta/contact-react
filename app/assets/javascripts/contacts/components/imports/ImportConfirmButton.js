import React from 'react';

import { confirmImport } from '../../actions/contacts';

class ImportConfirmButton extends React.Component {
  handleConfirm(e) {
    e.preventDefault();

    const { uuid, dispatch } = this.props;

    dispatch(confirmImport(uuid));
  }

  render() {
    return (
      <a
        onClick={::this.handleConfirm}
        href="#"
        className="btn waves-effect waves-light pull-right">
        Finish import
        </a>
    )
  }
}

export default ImportConfirmButton;
