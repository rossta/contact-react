import React from 'react';

import Header from '../../components';
import logger from '../../utils/logger';
import { uploadFile } from '../../actions/imports';

class ImportUploadForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {fileIsAttached: props.fileIsAttached || false};
  }

  handleSubmit(e) {
    e.preventDefault();

    const { dispatch } = this.props;
    const input = e.target.querySelector("#file");

    if (!input.value) {
      logger.warn('No file path for input');
      return false;
    }

    const file = input.files[0];

    if (!file) {
      logger.warn('No file attached');
      return false;
    }

    return dispatch(uploadFile(file));
  }

  fileDidAttach(e) {
    const input = e.target;
    const file = input.files[0];
    if (file.size > 1e+8) {
      input.value = "";
      this.setState({ error: "Sorry, max file size is 100MB. Please try a smaller file.", fileIsAttached: false });
    } else {
      this.setState({ error: null, fileIsAttached: true });
    }
  }

  isDisabled() {
    const { fileIsAttached, error } = this.state;

    return error || !fileIsAttached;
  }

  render() {
    const { error } = this.state;
    return (
      <div className="row">
        <h4>Import Contacts</h4>
        <p>Choose the file containing the contacts you want to import. In the next step, you'll be able to map each column to a field in ContactReact.</p>
        <p>Need help getting started? Try uploading the <a href="/data.tsv">sample contacts file</a> to give the app a test run.</p>
        <form onSubmit={::this.handleSubmit} className="col l12 s12">
          <div className="row">
            <div className="input-field col l3 s12">
              <input
                accept=".tsv"
                id="file"
                name="file"
                type="file"
                onChange={::this.fileDidAttach}
                />
            </div>
            <div className="input-field col l9 s12">
            {
              error &&
                <span className="error">{error}</span>
            }
            </div>
          </div>
          <div className="row">
            <div className="col s12 input-field">
              <input
                type="submit"
                className="btn waves-effect"
                value="Upload"
                disabled={this.isDisabled()}
                />
            </div>
          </div>
        </form>
      </div>
    )
  }
}

export default ImportUploadForm;
