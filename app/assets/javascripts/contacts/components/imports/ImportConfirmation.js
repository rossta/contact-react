import React from 'react';
import { Link } from 'react-router';

import { fetchImportIfNeeded } from '../../actions/imports';
import {
  Header,
  ContactRow,
  ImportConfirmButton,
} from '../../components';

class ImportConfirmation extends React.Component {
  componentDidMount() {
    const { dispatch, params } = this.props;

    dispatch(fetchImportIfNeeded(params.uuid));
  }

  renderContacts() {
    const { pendingContacts } = this.props;

    return (
      <table id="contacts-table" className="bordered responsive-table">
        <thead>
          <tr>
            <th className="data-field">First name</th>
            <th className="data-field">Last name</th>
            <th className="data-field">Email address</th>
            <th className="data-field">Phone number</th>
            <th className="data-field">Company</th>
          </tr>
        </thead>
        <tbody>
          {pendingContacts.map(this.renderContact)}
        </tbody>
      </table>
    );
  }

  renderContact(contact, index) {
    return <ContactRow key={index} contact={contact} />;
  }

  render() {
    const { uuid, dispatch } = this.props;
    return (
      <div>
        <div className="row">
          <div className="col l12 s12">
            <Header>Please confirm import</Header>
            <p>Review that the contact info is correct before finishing the import at the botton.</p>
          </div>
        </div>

        <div className="row">
          <div className="col l12 s12">
            {this.renderContacts()}
          </div>
        </div>

        <div className="row margin-top">
          <div className="col l12 s12">
            <Link to="/contacts" className="pull-right margin-left btn secondary-btn">Cancel</Link>
            <ImportConfirmButton uuid={uuid} dispatch={dispatch} />
          </div>
        </div>
      </div>
    );
  }
}

export default ImportConfirmation;
