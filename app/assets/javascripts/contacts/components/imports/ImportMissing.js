import React from 'react';

import { Header } from '../../components';

const ImportMissing = () => {
  return (
    <div>
      <Header>Oops!</Header>
      <p>This import has either already been imported or doesn't exist in the system.</p>
    </div>
  );
}

export default ImportMissing;
