import React from 'react';

export default function Header({children}) {
  return (
    <header>
      <h3>{children}</h3>
    </header>
  );
}
