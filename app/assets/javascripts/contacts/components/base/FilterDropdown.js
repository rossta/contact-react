import React from 'react';

class FilterDropwdown extends React.Component {
  handleChange(e) {
    this.props.onChange(e);
  }

  render() {
    const { label, options, selectedValue } = this.props;
    const name = label.toLowerCase().replace(/\s+/, '-')

    return (
      <div>
        <label htmlFor={name}>{label}</label>&nbsp;
        <select
          name={name}
          value={selectedValue}
          defaultValue=""
          className="filter"
          onChange={::this.handleChange}
          >
          {options.map(([text, value], i) => {
            return <option key={i} value={value}>{text}</option>
          })}
        </select>
      </div>
    );
  }
}

export default FilterDropwdown;
