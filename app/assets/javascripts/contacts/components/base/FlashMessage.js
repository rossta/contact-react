import React from 'react';

class FlashMessage extends React.Component {
  componentDidMount() {
    Materialize.toast(this.props.notice, 4000, 'flash-message', this.didFinish.bind(this));
  }

  didFinish() {
    this.props.onFinish();
  }

  render() {
    return (<div></div>);
  }
}

export default FlashMessage;
