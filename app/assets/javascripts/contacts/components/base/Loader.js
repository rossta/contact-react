import React from 'react';

const Loader = ({children}) => {
  return (
    <div>
      <h5>Just a sec</h5>
      <p>{children}</p>
    </div>
  );
}

export default Loader;
