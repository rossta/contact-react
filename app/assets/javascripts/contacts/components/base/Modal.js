import React from 'react';
import { Link } from 'react-router';

class Modal extends React.Component {
  componentDidMount() {
    $('#modal')
    .modal({ dismissible: false })
    .modal('open');
  }

  componentWillUnmount() {
    this.requestClose();
  }

  handleClose(e) {
    e.preventDefault();
    const { dispatch, onClose } = this.props;
    this.requestClose();
    dispatch(onClose());
  }

  requestClose() {
    $('#modal').modal('close');
  }

  render() {
    return (
      <div id="modal" className="modal">
        <div className="modal-content">
          <p className="pull-right">
            <a href="#" onClick={::this.handleClose}>Close</a>
          </p>
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default Modal;
