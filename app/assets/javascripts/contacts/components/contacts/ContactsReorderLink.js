import _ from 'lodash';
import React from 'react';

import {
  reorderContacts
} from '../../actions/contacts';

const [ASC, DESC] = ['asc', 'desc'];

class ReorderLink extends React.Component {
  handleClick(e) {
    e.preventDefault();
    const { dispatch, property } = this.props;

    dispatch(reorderContacts(property, this.toggledDirection()));
  }

  getDirection() {
    const { order, property } = this.props;
    return order[property];
  }

  toggledDirection() {
    if (!this.getDirection()) return ASC;
    return this.isAsc() ? DESC : ASC;
  }

  isAsc() {
    return (this.getDirection() || ASC) === ASC;
  }

  isOrdering() {
    const { order, property } = this.props;

    return _.includes(Object.keys(order), property);
  }

  renderArrow() {
    if (!this.isOrdering()) return '';

    return ' ' + (this.isAsc() ? '▼' : '▲');
  }

  render() {
    const { label } = this.props;
    return <span><a href="#" onClick={::this.handleClick}>{label}{this.renderArrow()}</a></span>;
  }
}

export default ReorderLink;
