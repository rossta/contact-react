import React from 'react';

import { deleteContact } from '../../actions/contacts';

class ContactsDeleteButton extends React.Component {
  handleClick(e) {
    e.preventDefault();

    const { contact, dispatch } = this.props;

    dispatch(deleteContact(contact));
  }

  render() {
    return <a href="#"
              onClick={::this.handleClick}
              className="btn waves-effect waves-red secondary-content delete-btn"
              disabled={this.props.disabled || false}
              >Delete</a>
  }
}

export default ContactsDeleteButton;
