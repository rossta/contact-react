import React from 'react';
import { Link } from 'react-router';

const ContactsBlankSlate = (props) => {
  return <p>You don't have any contacts yet! <Link to="/contacts/import/new">Import your contact list</Link> to get started.</p>;
}

export default ContactsBlankSlate;
