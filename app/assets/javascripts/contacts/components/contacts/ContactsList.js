import _ from 'lodash';
import React from 'react';
import { Link } from 'react-router';

import { fetchContactsIfNeeded, fetchContacts } from '../../actions/contacts';
import { showFlashMessage } from '../../actions/dashboard';

import {
  hasPhoneNumberInternational,
  hasPhoneNumberExtension,
  hasDotComEmailAddress
} from '../../utils';

import logger from '../../utils/logger';

import {
  Loader,
  ContactRow,
  ContactsBlankSlate,
  ContactsDeleteButton,
  ContactsReorderLink,
} from '../../components';

class ContactsTable extends React.Component {
  componentDidMount() {
    const { dispatch, channel } = this.props;

    dispatch(fetchContactsIfNeeded(channel));

    channel.bind('import_confirmed', json => dispatch(fetchContacts()));
    channel.bind('import_confirmed', json => dispatch(showFlashMessage(json.notice)));
  }

  componentWillUnmount() {
    const { channel } = this.props;

    channel.unbind('import_confirmed');
  }

  getVisibleContacts(contacts) {
    const { filter } = this.props;

    switch(filter) {
      case 'phone_number_international':
        return _.filter(contacts, hasPhoneNumberInternational);
      case 'phone_number_extension':
        return _.filter(contacts, hasPhoneNumberExtension);
      case 'email_address_dot_com':
        return _.filter(contacts, hasDotComEmailAddress);
      default:
        return contacts;
    }
  }

  getOrderedContacts(contacts) {
    const { order } = this.props;

    logger.log('order', order);
    if (_.isEmpty(order)) return contacts;

    const [properties, directions] = _(order).toPairs().unzip().value();

    logger.log('orderBy', properties, directions);
    return _.orderBy(contacts, properties, directions);
  }

  renderReorderLink(label, property) {
    const { order, dispatch } = this.props;
    const props = { order, dispatch, label, property };

    return <ContactsReorderLink {...props} />;
  }

  renderContact(contact, index) {
    const { dispatch } = this.props;
    const props = { contact, dispatch };

    return <ContactRow key={index} { ...props } />;
  }

  renderContactsTable() {
    const { contacts, order } = this.props;
    let displayContacts;
    displayContacts = this.getVisibleContacts(contacts);
    displayContacts = this.getOrderedContacts(displayContacts);

    return (
      <table id="contacts" className="contacts-table bordered responsive-table">
        <thead>
          <tr>
            <th className="data-field">{this.renderReorderLink('First name', 'first_name')}</th>
            <th className="data-field">{this.renderReorderLink('Last name', 'last_name')}</th>
            <th className="data-field">{this.renderReorderLink('Email address', 'email_address')}</th>
            <th className="data-field">Phone number</th>
            <th className="data-field">{this.renderReorderLink('Company', 'company_name')}</th>
            <th className="data-field"> </th>
          </tr>
        </thead>
        <tbody>
          {displayContacts.map(this.renderContact.bind(this))}
        </tbody>
      </table>
    );
  }

  render() {
    const { isImporting, isFetching, contacts } = this.props;

    if (isImporting) {
      return <Loader>Your contacts are processing...</Loader>;
    }
    else if (isFetching) {
      return <Loader>Fetching your contacts</Loader>;
    }
    else if (_.isEmpty(contacts)){
      return <ContactsBlankSlate />;
    }
    else {
      return this.renderContactsTable();
    }
  }
}

export default ContactsTable;
