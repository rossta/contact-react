import React from 'react';

import { FilterDropdown } from '../../components';
import { filterContacts } from '../../actions/contacts';

class ContactsFilterDropdown extends React.Component {
  onFilterChange(e) {
    const filter = e.target.value;
    const { dispatch } = this.props;

    dispatch(filterContacts(filter));
  }

  getFilterOptions() {
    return [
      ['All', 'all'],
      ['Phone number: international', 'phone_number_international'],
      ['Phone number: extension', 'phone_number_extension'],
      ['Email address: .com', 'email_address_dot_com'],
    ];
  }

  render() {
    return <FilterDropdown
            label="Filter contacts"
            options={::this.getFilterOptions()}
            onChange={::this.onFilterChange}
            />;
  }
}

export default ContactsFilterDropdown;
