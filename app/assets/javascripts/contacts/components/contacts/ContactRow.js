import React from 'react';

import { ContactsDeleteButton } from '../../components';

class ContactRow extends React.Component {
  render() {
    const { contact, dispatch } = this.props;

    return (
      <tr className="contact">
        <td>{contact.first_name}</td>
        <td>{contact.last_name}</td>
        <td>{contact.email_address}</td>
        <td>{contact.phone_number_formatted}</td>
        <td>{contact.company_name}</td>
        {
          dispatch &&
          <td>
            <ContactsDeleteButton contact={contact} dispatch={dispatch} />
          </td>
        }
      </tr>
    );
  }
}

export default ContactRow;
