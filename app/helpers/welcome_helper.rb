module WelcomeHelper
  def render_readme
    # rubocop:disable Rails/OutputSafety
    Rails.cache.fetch('readme') do
      markdown.render(File.read(Rails.root.join('README.md'))).html_safe
    end
    # rubocop:enable Rails/OutputSafety
  end
end
