class ConfirmImportJob < ActiveJob::Base
  queue_as :default

  def perform(import)
    if import.confirm
      notice = "Ding! #{import.length} contacts have now been imported."
      trigger('import_confirmed', import, notice: notice)
    else
      notice = 'Sorry! There was a problem and your contacts were not imported.'
      trigger('import_failed', import, notice: notice)
    end
  end

  private

  def trigger(event, import, opts)
    payload = opts.merge uuid: import.uuid, size: import.length
    push_client.trigger('contacts_channel', event, payload)
  end

  def push_client
    @push_client ||= PushClient.new
  end
end
