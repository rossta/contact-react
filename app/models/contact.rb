# Represents contact info for an individual
class Contact < ActiveRecord::Base
  include HasContactInfo

  # Creates a set of contacts from a given Import. Leverages the self.import
  # method provided by activerecord-import to ingest batch more efficiently.
  #
  # @param import [Import] the Import to ingest as Contact records
  # @return [Boolean] true if success, false if failure
  #
  def self.create_from_import(import)
    return false unless import.valid?

    transaction do
      Contact.import(import.column_names, import.values, validate: false)
      import.finalize!
    end
  end
end
