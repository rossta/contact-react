# PORO model to represent a single pending contact extracted from an Import.
# Data can act as source for creating a Contact record
#
class PendingContact
  include PlainModel
  include HasContactInfo

  # Provides the public-facing column names for listing contacts
  #
  # @return [Array<String>] the list of column names
  #
  def self.column_names
    display_column_names
  end

  attr_accessor(*column_names)

  def [](attr)
    send(attr)
  end

  # Returns the attribute values as a collection
  #
  # @return [Array] the attribute values
  #
  def values
    column_names.map { |method| send(method) }
  end

  # Returns the attributes by display column order
  #
  # @return [Hash] the ordered attributes
  #
  def to_hash
    Hash[columns.zip(values)]
  end

  # Returns the column names for the contact data
  #
  # @return [Array<String>] the column names
  #
  def column_names
    self.class.column_names
  end
end
