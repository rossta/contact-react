require 'csv'
require 'securerandom'

# PORO model to represent a collection of pending contacts extracted from a single file upload.
# Data is stored in Rails.cache, keyed by a UUID generated at creation.
# Implements the ActiveModel interface for handling attributes, callbacks, and json serialization.
#
class Import
  include PlainModel
  include GlobalID::Identification

  attr_accessor :uuid, :pending_contacts, :finalized

  delegate :size, :length, :first, :last, :each, to: :pending_contacts

  define_model_callbacks :initialize, only: :after
  after_initialize :cast_pending_contacts

  # Creates an Import instance from a tab-separated file of contact
  # data cast to a collection of pending contacts.
  #
  # @param file [File] the file to parse and import
  # @return [Import] the newly created Import instance
  #
  def self.from_file(file)
    csv_file = CSV.read(file.path, col_sep: "\t", headers: true)
    contacts = csv_file.map do |row|
      row.to_h.each_with_object({}) do |(k, v), h|
        h[k.parameterize.underscore] = v
      end.with_indifferent_access
    end

    create(pending_contacts: contacts)
  end

  # Creates an Import instance a collection of pending contacts
  #
  # @param [Hash] the attributes
  # @option :pending_contacts [Array<Hash,PendingContact>] the collection of pending contact data
  # @return [Import] the newly created Import instance
  #
  def self.create(pending_contacts: [])
    uuid = SecureRandom.uuid
    import = Import.new(uuid: uuid, pending_contacts: pending_contacts)
    import.save
    import
  end

  # Locates an Import instance from Rails.cache by UUID.
  # Raises an ActiveRecord::RecordNotFound when no import for the given UUID exists.
  #
  # @param uuid [String] the uuid of the Import to find
  # @return [Import] the newly created Import instance
  #
  def self.find(uuid)
    (attrs = Rails.cache.read(['import', uuid])) ||
      raise(ActiveRecord::RecordNotFound, "Could not find an import with uuid #{uuid}")

    Import.new(attrs)
  end

  # Initializes an Import instance from a collection of pending contacts
  #
  # @param attrs [*Hash] the collection of attributes to import
  # @return [Import] the newly initialized Import instance
  #
  def initialize(*attrs)
    super

    run_callbacks :initialize
  end

  # Alias uuid for GlobalID support and ActiveJob serialization
  #
  # @return [String] the uuid
  def id
    uuid
  end

  # Provides the public-facing column names for listing contacts
  #
  # @return [Array<String>] the list of column names
  #
  def column_names
    PendingContact.column_names
  end

  # Provides the raw data for the collection of pending contacts
  #
  # @return [Array<Array>] the collection of contact data
  #
  def values
    pending_contacts.map(&:values)
  end

  # Validates all pending contacts
  #
  # @return [Boolean] validity of all pending contacts
  #
  def valid?
    pending_contacts.all?(&:valid?)
  end

  # Creates contact records from pending contact data idempotently.
  #
  # @return [Boolean] success/failure of confirmation
  #
  def confirm
    return true if finalized?
    return false unless valid?

    Contact.create_from_import(self)
  end

  # Saves the import idempotently
  #
  # @return [Boolean] success/failure of saving
  #
  def finalize!
    @finalized = true
    save
  end

  # Returns finalized status
  #
  # @return [Boolean] finalized or not
  #
  def finalized?
    @finalized
  end

  # Serializes the import data as json to Rails.cache
  #
  # @return [Boolean] success/failure of write
  #
  def save
    Rails.cache.write(['import', uuid], as_json(except: :global_id))
  end

  private

  def cast_pending_contacts
    @pending_contacts = pending_contacts.map { |attrs| to_pending_contact(attrs).tap(&:valid?) }
  end

  def to_pending_contact(obj)
    if obj.is_a?(PendingContact)
      obj
    else
      PendingContact.new(obj)
    end
  end
end
