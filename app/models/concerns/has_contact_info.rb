module HasContactInfo
  extend ActiveSupport::Concern

  included do
    # Provides the public-facing column names for listing contacts
    #
    # @return [Array<String>] the list of column names
    #
    class_attribute :display_column_names
    self.display_column_names = %w(first_name last_name email_address phone_number company_name)

    phony_normalize :phone_number, default_country_code: 'US'
    attr_accessor :phone_number_as_normalized

    validate :identifier_present?
    validates :email_address, format: /@/, allow_blank: true
    validates :phone_number_as_normalized, phony_plausible: true
  end

  # Normalized, formatted phone number
  #
  # @return [String] the formatted phone number
  #
  def phone_number_formatted
    phone_number.phony_formatted(format: :international, spaces: '-')
  end

  private

  def identifier_present?
    if attributes.slice(*%w(first_name last_name email_address)).values.all?(&:blank?)
      errors.add :base, 'Please provide at least one: First name, Last name, Email address'
    end
  end
end
