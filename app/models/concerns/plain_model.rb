module PlainModel
  extend ActiveSupport::Concern

  included do
    include ActiveModel::Model
    include ActiveModel::Serializers::JSON
    include ActiveModel::Validations::Callbacks
  end

  # Satisifies the ActiveModel#attributes= interface for setting attributes on the Import
  #
  # @param hash [Hash] the hash of attributes to set
  # @return [Hash]
  #
  def attributes=(hash)
    hash.each do |key, value|
      send("#{key}=", value)
    end
  end

  # Satisifies the ActiveModel#attributes interface for getting attributes from the Import
  #
  # @return [Hash] the attributes
  #
  def attributes
    instance_values
  end
end
