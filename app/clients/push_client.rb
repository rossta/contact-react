# Wrapper class for Pusher gem client: "stub what you own"
class PushClient
  def trigger(*args)
    if Rails.application.config.pusher_enabled
      pusher_client.trigger(*args)
    else
      null_client.trigger(*args)
    end
  end

  def pusher_client
    Pusher::Client.new(
      app_id: ENV.fetch('PUSHER_APP_ID'),
      key: ENV.fetch('PUSHER_KEY'),
      secret: ENV.fetch('PUSHER_SECRET'),
      encrypted: true
    )
  end

  def null_client
    NullClient.new
  end

  class NullClient
    def trigger(*args)
      Rails.logger.info("Stubbing push reqest for #{args.inspect}")
    end
  end
end
